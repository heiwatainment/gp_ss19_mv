﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeworkAB : MonoBehaviour
{

    [SerializeField] float speed = 1.0f;
    [SerializeField] float maxFactor = 2;

    Vector3 currentPos;
    Vector3 minPos;
    Vector3 maxPos;
    bool transfer = false;





    // Start is called before the first frame update
    void Start()
    {
        currentPos = gameObject.transform.localScale;
        minPos = currentPos;
        maxPos = new Vector3(minPos.x * maxFactor, minPos.y * maxFactor, minPos.z * maxFactor);








    }

    // Update is called once per frame
    void Update()
    {
        if (transfer == false)
        {
            if (currentPos.x <= maxPos.x)

            {
                currentPos = currentPos + new Vector3(speed, speed, speed) * Time.deltaTime;
                gameObject.transform.localScale = currentPos;
            }
            else
            {
                transfer = true;
            }
        }

        else
            if (transfer == true)
        {
            if (currentPos.x >= minPos.x)

            {
                currentPos = currentPos - new Vector3(speed, speed, speed) * Time.deltaTime;
                gameObject.transform.localScale = currentPos;

            }
            else
            {
                transfer = false;
            }


        }

    }
}
    

