﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadAfterFall : MonoBehaviour
{
    Vector3 fall;
    [SerializeField] float border = -100;
    
    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        Vector3 fall = gameObject.transform.position;

        if (fall.y <= border)
        {
            SceneManager.LoadScene("Homework E");
        }
    }
}
