﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTrigger : MonoBehaviour
{
    [SerializeField] string levelToLoad = "Homework E";
    AudioSource lvlClear;


    void OnTriggerEnter(Collider other)

    {
        lvlClear = gameObject.GetComponent<AudioSource>();
        lvlClear.Play();
        Invoke("LoadLevel", 1.0f);
    }

    void LoadLevel()
    {
        SceneManager.LoadScene(levelToLoad);
        
    }
}
