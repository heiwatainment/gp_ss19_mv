﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpening : MonoBehaviour
{
    [SerializeField] GameObject Door;
    public bool open = false;
    [SerializeField] float speed = -0.1f;

    public void OnTriggerEnter(Collider other)
    {
        open = true;

    }

    void Update()
    {
        if (open == true && Door.transform.position.y > -4)
        {
            Door.transform.position += new Vector3(0, speed, 0);
        }
    }

    
    
}
