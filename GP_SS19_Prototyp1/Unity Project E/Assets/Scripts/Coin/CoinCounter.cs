﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinCounter : MonoBehaviour
{

    [SerializeField] Text CoinDisplay;
    public int coins_col;
    public int coins_max;

    public void Start()
    {
        

        coins_max = GameObject.FindGameObjectsWithTag("Coin").Length;
    }

    void Update()
    {
         
        CoinDisplay.text = "Coins: " + coins_col + "/" + coins_max;
    }

    public void OnTriggerEnter(Collider target)
    {
        if(target.gameObject.tag.Equals("Coin") == true)
        {
            coins_col++;
        }
        
    }

}
