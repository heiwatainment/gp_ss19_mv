﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpProperty : MonoBehaviour
{
    [SerializeField] AudioSource pickupSound;

    void Start()
    {
       
        pickupSound = GameObject.FindGameObjectWithTag("coin_snd").GetComponent<AudioSource>();
       
        if (pickupSound == null) { Debug.Log("No Audiosource found in PickupProperty."); }     
    }



    void OnTriggerEnter(Collider other)
    {
        pickupSound.Play();
        Destroy(gameObject);

    }

 


  

   

}
