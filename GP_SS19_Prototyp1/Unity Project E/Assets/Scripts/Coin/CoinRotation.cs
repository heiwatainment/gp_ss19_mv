﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinRotation : MonoBehaviour
{

    Vector3 m_eulerAngles;
     [SerializeField] float rotationSpeed = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        //saves the current angle of the GameObject in a variable
        m_eulerAngles = gameObject.transform.rotation.eulerAngles;
        
    }

    // Update is called once per frame
    void Update()
    {
        m_eulerAngles.y = m_eulerAngles.y + rotationSpeed * Time.deltaTime; 

        gameObject.transform.rotation = Quaternion.Euler(m_eulerAngles);
    }
}
