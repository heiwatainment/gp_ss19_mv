﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossRotation : MonoBehaviour
{

    Vector3 m_eulerAngles;
    [SerializeField] float xRotation = 10.0f;
    [SerializeField] float yRotation = 10.0f;
    [SerializeField] float zRotation = 10.0f;
    [SerializeField] bool xAxis = true;
    [SerializeField] bool yAxis = false;
    [SerializeField] bool zAxis = false;

    // Start is called before the first frame update
    void Start()
    {
        //saves the current angle of the GameObject in a variable
        m_eulerAngles = gameObject.transform.rotation.eulerAngles;

    }

    // Update is called once per frame
    void Update()
    {
        
      
        if (xAxis == true)
        {
            m_eulerAngles.x = m_eulerAngles.x + xRotation * Time.deltaTime;

            gameObject.transform.rotation = Quaternion.Euler(m_eulerAngles);
        }

        if (yAxis == true)
        {
            m_eulerAngles.y = m_eulerAngles.y + yRotation * Time.deltaTime;

            gameObject.transform.rotation = Quaternion.Euler(m_eulerAngles);
        }

        if (zAxis == true)
        {
            m_eulerAngles.z = m_eulerAngles.z + zRotation * Time.deltaTime;

            gameObject.transform.rotation = Quaternion.Euler(m_eulerAngles);
        }
    }
}
