﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalMovementController : MonoBehaviour
{

    [SerializeField] float m_movementSpeed = 5;
    [SerializeField] float jumpSpeed =100;
    Rigidbody m_myRigidBody;

    // Start is called before the first frame update
    void Start()
    {
        m_myRigidBody = gameObject.GetComponent<Rigidbody>();
        if (m_myRigidBody == null)
        {
            Debug.Log("No RigidBody found in PhysicalMovementController");
        }
    }

    // Update is called once per frame
    void Update()
    {
        // gets current input.position
        float horizontalMovement = Input.GetAxis("Horizontal");
        float verticalMovement = Input.GetAxis("Vertical");

        //creating a direction vector for the force
        Vector3 movementForce = new Vector3();

        movementForce.x = horizontalMovement * m_movementSpeed * Time.deltaTime;
        movementForce.z = verticalMovement * m_movementSpeed * Time.deltaTime;


      



    

       

        if (Input.GetButtonDown("Jump"))
        {
            movementForce.y =  jumpSpeed * Time.deltaTime;
        }
        //and add tp force as an impulse to the rigidbody
        m_myRigidBody.AddForce(movementForce, ForceMode.Impulse);



    }
}
