﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MovementControl : MonoBehaviour
{


   
    [SerializeField] float jumpVelocity = 7f;
    [SerializeField] float movementSpeed = 15f;

    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2.0f;

    AudioSource jumpingSound;
    Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        if (rb == null)
        {
            Debug.Log("No RigidBody found in PhysicalMovementController");
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 desiredPosition = gameObject.transform.position;

        float horizontalMovement = Input.GetAxis("Horizontal");
        desiredPosition.x = desiredPosition.x + horizontalMovement * movementSpeed * Time.deltaTime;
        gameObject.transform.position = desiredPosition;

        
        if(Input.GetButtonDown("Jump"))
        {
            jumpingSound = gameObject.GetComponent<AudioSource>();
            jumpingSound.Play();
            GetComponent<Rigidbody>().velocity = Vector3.up * jumpVelocity;
        }


        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        } else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }



}
}
