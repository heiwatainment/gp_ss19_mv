﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [SerializeField] float movementSpeed = 100.0f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //saves current position of gameobject
        Vector3 desiredPosition = gameObject.transform.position;

        //gets the horizontal axes if WASD or arrow keys were pressed
        // The value is -1 for left and +1 for right
        float horizontalMovement = Input.GetAxis("Horizontal");

        //calculating new position
        desiredPosition.x = desiredPosition.x + horizontalMovement * movementSpeed * Time.deltaTime;

        //saving new position 
        gameObject.transform.position = desiredPosition;


        // Y-axis

        //saves current position of gameobject
        Vector3 desiredPositionZ = gameObject.transform.position;

        //gets the horizontal axes if WASD or arrow keys were pressed
        // The value is -1 for left and +1 for right
        float verticalMovement = Input.GetAxis("Vertical");

        //calculating new position
        desiredPositionZ.z = desiredPositionZ.z + verticalMovement * movementSpeed * Time.deltaTime;

        //saving new position 
        gameObject.transform.position = desiredPositionZ;
    }
}
