﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class HangmanManager : MonoBehaviour
{
    //Declaring Text Variable which displays a string, which can be changed within later code.
    [SerializeField] Text m_mainText;
    //Declaring an Inputfield, which stores a word as a string.
    [SerializeField] InputField m_wordInputField;
    //Decaring an Inputfield, which stores one character. Will be used to compare the letter with the hidden word.
    [SerializeField] InputField m_charInputfield;
    //Declaring a Button, which on click will start a method to restart the Game.
    [SerializeField] Button m_restartButton;
    //Declaring a Button, which on click will start a method to start the HotSeatGame.
    [SerializeField] Button m_startButton;
    //Declaring a Button, which on click will start a method to start the SingleplayerGame
    [SerializeField] Button m_singleplayerButton;
    //Declaring an Image, which displays the hangman image in the Canvas.
    [SerializeField] Image m_hangmanImage;
    //Declaring a Sprite Array, which will store the hangman images. Used to change the Image Variables for the hangman images.
    [SerializeField] Sprite[] m_picture_array;
    //Declaring an Skull image, which will be displayed when the player fails to win.
    [SerializeField] Image m_skull;
    //Declaring an Image Star, which will be used to be displayed, when the player wins the game.
    [SerializeField] Image m_star;
    //Declaring a Text, which stores strings to be displayed in the canvas, so the player can see, which letters have already been used.
    [SerializeField] Text lettersUsed;
    //Declaring a Button Exit, which just quits the application (Does only work in Build, not in Editor mode!)
    [SerializeField] Button exit;
    //Declaring an array string, storing the alphabet, which will be used at random if the player's Input for guessing a character is null.
    string[] randomizedLetter = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

    //Declaring three strings, which can be edited in the editor. Used for Singleplayer mode, so one of three words can be chosen at random for guessing. 
    //Will not be used, though, as we have an array list of words to be guessed.
    [SerializeField] string m_singlePlayerWord1;
    [SerializeField] string m_singlePlayerWord2;
    [SerializeField] string m_singlePlayerWord3;
    //Declaring an array string, which holds x (for now, 100 words have been stored) amount of strings, which will be used for singleplayer mode, so the player can guess a word at random.
    [SerializeField] string[] m_singleplayerWord;
    
    
    //Declaring a string, which stores the word which the player has to guess. In Singleplayermode, this string will store one string from the array list above.
    //In Hotseatgame, the player fills the string via Input.
    string m_wordToGuess;
    //Declaring a string, which will display the word with underscores.
    string m_wordToDisplay;
    //Declaring a string, which stores a character via players Input.
    string m_characterToGuess;
    //Declaring an integer length, which will be used to determine the lenght for a string.
    int length;
    //Declaring a boolean singleplayer, which will be used to check if player is in singleplayer or hotseatgame mode.
    bool singleplayer;
   
    //Declaring an integer with the maximum of total guesses a player can have
    [SerializeField] int m_maximalWrongGuesses = 6;
    //Declaring an integer which stores the amount of wrong guesses the player already did.
    int m_numberOfWrongGuesses;
   
//Method Start() will be executed once, when the application starts.
    void Start()
    {
        //In the start method, the SetupGame() method will be executed.
        SetupGame();
    }

//a public method, which is used as a default screen within the game, so the player can reset to the start of the game without loading the scene a second time
    public void SetupGame()

    {
        //Setting up the default screen, by hiding objects via SetActive(false) and displaying them via SetActive(True).
        exit.gameObject.SetActive(true);
        m_star.gameObject.SetActive(false);
        lettersUsed.gameObject.SetActive(false);
        m_charInputfield.gameObject.SetActive(false);
        m_wordInputField.gameObject.SetActive(false);
        m_restartButton.gameObject.SetActive(false);
        m_startButton.gameObject.SetActive(true);
        m_skull.gameObject.SetActive(false);
        m_singleplayerButton.gameObject.SetActive(true);

        //Adding a string to the text variable by accesing the text variable with ".text"
        lettersUsed.text = "Letters used:\n";

        //Setting the singleplayer bool on default, which is in this case 'false'.
        singleplayer = false;
        //Setting the number of wrong guesses on default, which is in this case 0.
        m_numberOfWrongGuesses = 0;

        //Setting the word to guess as default with an empty string.
        m_wordToGuess = "";
        ////Setting the word to display as default with an empty string.
        m_wordToDisplay = "";

        //Setting the maintext to default with a string, displayed at the beginning of the game
        m_mainText.text = "Welcome to <b>Hangman</b>!\n\n";
        m_mainText.text += "Choose a Game Mode!";

 
        //Setting the hangman image to default by accessing the sprite array list
        m_hangmanImage.sprite = m_picture_array[0];


    }

    
    public void WordEntered()
    {
        //Method to check if singleplayer mode is active or not (see more in the method)
        WordTransform();

        //Variable's text will be changed to an empty string
        m_wordInputField.text = "";

        //Variable's text will be changed to an empty string
        m_wordToDisplay = "";

        //Starts method to hide the players Input (used for Hotseatgame) - Hides the word, chosen by the players input
        GenerateHiddenText();

        //Hides the Input field as the player already did an input for the word to guess
        m_wordInputField.gameObject.SetActive(false);
        //Making the input for the character to guess visible
        m_charInputfield.gameObject.SetActive(true);

        //Changing the MainText
        UpdateMainText("Lets start the game!");

        
    }
    //Generates a word that has the same length as the word to guess,
    //but only contains underline characters
    void GenerateHiddenText()
    {

        //Word to display is an empty string
        m_wordToDisplay = "";

        //using a for loop to check each character char in the string m_wordToGues
        foreach (char character in m_wordToGuess)
        {
            //If the character is storing space, , the String WordToDIsplay adds space as well
            if (character == ' ')
            {
                m_wordToDisplay += " ";
            }
            else
            //If thee character is not a space, an underscore is added to the string wordToDisplay
            m_wordToDisplay += "_";
        }

    }

    void UpdateMainText(string _startText)
    {
        //Hiding exit button
        exit.gameObject.SetActive(false);

        //Selecting the Inputfield as default
        m_charInputfield.Select();
        //and making the Inputfield visible
        m_charInputfield.gameObject.SetActive(true);
        //Also Activating it.
        m_charInputfield.ActivateInputField();


        //Changing the Maintext to _startext and adding a wordwrap
        m_mainText.text = _startText + "\n\n";
        m_mainText.text += "You have to guess the word: <b>" + m_wordToDisplay + "</b>";
   
        m_mainText.text += "\n\n" + "Enter a character that you want to guess...";
        //Displaying variables within the string, so they can be displayed
        m_mainText.text += "\n\n" + "You can try " + (m_maximalWrongGuesses - m_numberOfWrongGuesses) + " times!";

    }

    //Method checking which character is given via input and comparing it with the word to guess
    public void CharacterEntered()
    {
        //If the player is did not use anything for input, or the input is null, a random letter will be chosen
        if (m_charInputfield.text == "")
        {
            //Determining a random range the integer randomLetter can have
            int randomLetter = Random.Range(0,26);
            //randomLetter chooses a string from the array randomized letter and use it as input instead
            m_charInputfield.text = randomizedLetter[randomLetter];
        }
            //the character to guess will store the players input and changing the input to upper letters
            m_characterToGuess = m_charInputfield.text.ToUpper();
            //The Input field will be set to default again.
            m_charInputfield.text = "";


            //Declaring an integer index storing '0'
            int index = 0;
            //Declaring a boolean charaterFound to default which is in this case false
            bool characterFound = false;
            
            //Doing a for loop for each char character in the word to guess
            foreach (char character in m_wordToGuess)
            {
                //comparision of the character and the word to guess, using array system, so each character can be compared, if there is a match, the statement is executed
                if (character == m_characterToGuess[0])
                {
                    m_wordToDisplay = m_wordToDisplay.Remove(index, 1);
                    m_wordToDisplay = m_wordToDisplay.Insert(index, character.ToString());

                    characterFound = true;
                }
                //index integer +1
                index++;
            }

            //If characterFound == true, execute this
            if (characterFound)
            {
                //Updating Main Text
                UpdateMainText("You entered <b>" + m_characterToGuess + "</b> and guessed right!");

                //If the word has been found, execute the GameWon() method - comparing to strings, if they are equal, execute.
                if (m_wordToGuess == m_wordToDisplay)
                {
                    GameWon();
                }
            }
            else //If they don't match, do this:
            {
                //Setting the game object lettersUsed to visible -- displaying letters already used
                lettersUsed.gameObject.SetActive(true);

                //Increasing the number of wrong guesses by one
                m_numberOfWrongGuesses++;
                //Changing the hangman sprite's image by the number of wrong guesses
                m_hangmanImage.sprite = m_picture_array[m_numberOfWrongGuesses];

                //changing text for letters used, just added semicolons, to keep the track better
                lettersUsed.text += m_characterToGuess + ", ";

                //If the wrong guesses euqal the amount of maximum guesses the player have, execute this
                if (m_numberOfWrongGuesses == m_maximalWrongGuesses)
                {
                    //GameOver Methid will be executed
                    GameOverText();
                }
                else //If not equal do this:
                {
                    //Updating Maintext, to show that the player guessed wrong
                    UpdateMainText("You entered character: <b>" + m_characterToGuess + "</b> and guessed wrong!");
                }
            }
        
    }

    void GameOverText()
    {
        //Setting skull image object to visible, animated via Animator
        m_skull.gameObject.SetActive(true);
        //Changing Main Text
        m_mainText.text = "You entered <b>" + m_characterToGuess + "</b> and guessed wrong for the last time!";
        m_mainText.text += "\n\n" + "You are dead! The hidden word was: " + "<b>" + m_wordToGuess + "</b>";
        m_mainText.text += "\n\n" + "GAME OVER!";

        //Setting Objects to visible and not visible 
        m_charInputfield.gameObject.SetActive(false);
        m_restartButton.gameObject.SetActive(true);
        lettersUsed.gameObject.SetActive(false);
    }

    //method executed, when player wins the game
    void GameWon()
    {
        //Changung maintext
        m_mainText.text = "You entered <b>" + m_characterToGuess + "</b> and guessed right!";
        m_mainText.text += "\n\n" + "Congratulations, you guessed the word <b>" + m_wordToDisplay +  " </b>correctly!";
        m_mainText.text += "\n\n" + "Play again?";
      
        //Changing Objects to visible/Not visible
        m_charInputfield.gameObject.SetActive(false);
        m_restartButton.gameObject.SetActive(true);

        m_star.gameObject.SetActive(true);
        lettersUsed.gameObject.SetActive(false);


    }

    public void StartHotSeatGame()    
    {
        //Game objects: visible/Not visible
        exit.gameObject.SetActive(false);
        m_charInputfield.gameObject.SetActive(false);
        m_restartButton.gameObject.SetActive(false);
        m_wordInputField.gameObject.SetActive(true);
        m_startButton.gameObject.SetActive(false);
        m_singleplayerButton.gameObject.SetActive(false);
        m_hangmanImage.gameObject.SetActive(true);

        //selecting Input by default
        m_wordInputField.Select();

        //Sprite greift auf Array zu und nimmt sich Sprite 0 aus dem Array
        //Hangman image set to the array list 0
        m_hangmanImage.sprite = m_picture_array[0];

        //Changing maintext
        m_mainText.text = "Please enter a word to guess!";

        
    }



    public void StartSingleplayer()
    {
        //Game objects visible/notvisible
        m_singleplayerButton.gameObject.SetActive(false);
        m_startButton.gameObject.SetActive(false);

        //integer with random range to choose from the stored words in the array singleplayerword
        int randomNumber = Random.Range(0, 101);

        m_wordToGuess = m_singleplayerWord[randomNumber];
        //also, converting the string to upper letters again
        m_wordToGuess = m_wordToGuess.ToUpper();


        //Setting bool singleplayer to true, so that the hotseatgame will not be player
        singleplayer = true;

        //executing method, to hide the string with underscores
        GenerateHiddenText();
        //Another text changing
        UpdateMainText("The AI picked a word for you, try to guess it!");
        
       

    }

    public void WordTransform()
    {
        //used to check which playmode currently is played
        if (singleplayer == false)
        {
            
            m_wordToGuess = m_wordInputField.text.ToUpper();
            if(m_wordToGuess == "")
            {
                StartSingleplayer();
            }
        } 
        
    }

    //just exits the application. the exit gameobject will run this method
    public void exitGame()
    {
        Application.Quit();
    }
    
    //restarts the scene, this method is not used, as the SetupGame() method changes everything to default
    public void Restart()
    {
        SceneManager.LoadScene("Hangman");
    }
}
